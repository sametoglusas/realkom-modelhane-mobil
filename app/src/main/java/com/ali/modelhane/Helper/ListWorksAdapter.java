package com.ali.modelhane.Helper;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ali.modelhane.Main.Info;
import com.ali.modelhane.Main.DesignerActivity;
import com.ali.modelhane.Main.WorksFragment;
import com.ali.modelhane.Main.popupMiss;
import com.ali.modelhane.Main.popupinfo;
import com.ali.modelhane.R;

import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


public class ListWorksAdapter extends RecyclerSwipeAdapter<ListWorksAdapter.MyViewHolder> {

    public String [] orderdetails1 ;
    public String [] orderdetails2 ;
    public String [] orderdetails3 ;
    public String [] orderdetails4 ;
    public String [] orderdetails5 ;
    public String [] orderdetails6 ;
    public String [] orderdetails7 ;
    public String [] orderdetails8 ;
    public String [] orderdetails9 ;



    public Context ctx;

    public static int controlSwitcher = 0;

    public static int isclick = 0;

    public static String kayıpsaat = "0";

    WorksFragment wf = new WorksFragment();
    popupMiss pm = new popupMiss();

    DateFormat dateFormat = new SimpleDateFormat("HH:mm");
//    Date date = new Date();

    SharedPreferences preferences;
    SharedPreferences.Editor editor;


    final MyViewHolder holder;



    public ListWorksAdapter(Context ctx , String [] orderdetails1, String [] orderdetails2, String [] orderdetails3, String [] orderdetails4, String [] orderdetails5, String [] orderdetails6, String [] orderdetails7, String [] orderdetails8,String [] orderdetails9) {
        this.orderdetails1 = orderdetails1;
        this.orderdetails2 = orderdetails2;
        this.orderdetails3 = orderdetails3;
        this.orderdetails4 = orderdetails4;
        this.orderdetails5 = orderdetails5;
        this.orderdetails6 = orderdetails6;
        this.orderdetails7 = orderdetails7;
        this.orderdetails8 = orderdetails8;
        this.orderdetails9 = orderdetails9;


        this.ctx=ctx;
        holder = null;
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView lbl_critic,lbl_info,lbl_product,lbl_difficult,lbl_name,lbl_orderno,lbl_modelno;

        public ImageButton play,pause,stop,resume,annotation;

        public SwipeLayout swipeLayout;

        //public CardView cv_card;

        public RelativeLayout rl_card;

        MyViewHolder(View view) {
            super(view);
            lbl_critic = view.findViewById(R.id.txt_order_critic);
            lbl_info = view.findViewById(R.id.txt_order_info);
            lbl_product = view.findViewById(R.id.txt_order_product);
            lbl_difficult = view.findViewById(R.id.txt_order_difficult);
            lbl_name = view.findViewById(R.id.txt_model_name);
            lbl_orderno = view.findViewById(R.id.txt_order_no);
            lbl_modelno = view.findViewById(R.id.txt_model_no);
//            cv_card = view.findViewById(R.id.cv_card);
            rl_card = view.findViewById(R.id.rl_card);

            swipeLayout = itemView.findViewById(R.id.swipe);

            play = view.findViewById(R.id.play);
            pause = view.findViewById(R.id.pause);
            resume = view.findViewById(R.id.cont);
            stop = view.findViewById(R.id.stop);

            annotation = view.findViewById(R.id.anno);


        }
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.workcellview, parent, false);
        return new MyViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        preferences = PreferenceManager.getDefaultSharedPreferences(ctx);
        editor = preferences.edit();

        if (orderdetails1[position].equals("")){
            holder.lbl_critic.setText("Yok.");
            holder.play.setVisibility(View.GONE);
        }
        else {
            holder.lbl_critic.setText(orderdetails1[position]);
        }

        holder.lbl_info.setText(orderdetails2[position]);
        holder.lbl_product.setText(orderdetails3[position]);
        holder.lbl_difficult.setText(orderdetails4[position]);
        holder.lbl_name.setText(orderdetails5[position]);
        holder.lbl_orderno.setText(orderdetails6[position]);
        holder.lbl_modelno.setText(orderdetails7[position]);



        holder.stop.setVisibility(View.INVISIBLE);
        holder.resume.setVisibility(View.INVISIBLE);
        holder.pause.setVisibility(View.INVISIBLE);

        if (orderdetails8[position].contains("Duraklat")){
            holder.play.setVisibility(View.INVISIBLE);
            holder.resume.setVisibility(View.VISIBLE);

        }
        else {
            holder.play.setVisibility(View.VISIBLE);
        }

        if (orderdetails8[position].contains("Ask")){
            //holder.itemView.setBackgroundColor(Color.rgb(135, 155, 155));

            holder.play.setVisibility(View.INVISIBLE);
            holder.resume.setVisibility(View.VISIBLE);
//            holder.swipeLayout.setSwipeEnabled(false);
            holder.swipeLayout.setLeftSwipeEnabled(false);

//            holder.cv_card.setCardBackgroundColor(Color.rgb(135, 155, 155));
            holder.rl_card.setBackground(ctx.getResources().getDrawable(R.drawable.card_edgework_info));

        }
        else {

        }

        /**/
        holder.swipeLayout.setShowMode(SwipeLayout.ShowMode.PullOut);
        holder.swipeLayout.addDrag(SwipeLayout.DragEdge.Left, holder.swipeLayout.findViewById(R.id.bottom_wrapper1));
        holder.swipeLayout.addDrag(SwipeLayout.DragEdge.Right, holder.swipeLayout.findViewById(R.id.bottom_wraper));
        /**/

        SetScale(holder);



        holder.rl_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (orderdetails8[position].contains("Ask")){

                    Toast.makeText(ctx, "Bu ilanda Bilgi Eksikliği Vardır. İsterseniz işinize Devam Edebilirsiniz.", Toast.LENGTH_LONG).show();

                }
                else {

                }
            }
        });

        holder.play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isclick == 1){
                    Toast.makeText(ctx, "Su anda zaten bir siparişin ustunde calısıyorsunuz.", Toast.LENGTH_SHORT).show();
                }

                else {

                controlSwitcher = 0;
                isclick = 1;


                holder.pause.requestLayout();
                holder.pause.getLayoutParams().width = -2;
                holder.pause.setScaleType(ImageView.ScaleType.FIT_CENTER);
                holder.pause.setVisibility(View.VISIBLE);

                holder.play.setVisibility(View.INVISIBLE);
                holder.resume.setVisibility(View.INVISIBLE);

                holder.stop.requestLayout();
                holder.stop.getLayoutParams().width = -2;
                holder.stop.setScaleType(ImageView.ScaleType.FIT_CENTER);
                holder.stop.setVisibility(View.VISIBLE);

                workplay();

                }

            }
        });
        holder.resume.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isclick == 1){
                    Toast.makeText(ctx, "Su anda zaten bir siparişin ustunde calısıyorsunuz.", Toast.LENGTH_SHORT).show();
                }

                else {

                    isclick = 1;

                    holder.pause.requestLayout();
                    holder.pause.getLayoutParams().width = -2;
                    holder.pause.setScaleType(ImageView.ScaleType.FIT_CENTER);
                    holder.pause.setVisibility(View.VISIBLE);

                    holder.play.setVisibility(View.INVISIBLE);
                    holder.resume.setVisibility(View.INVISIBLE);

                    holder.stop.requestLayout();
                    holder.stop.getLayoutParams().width = -2;
                    holder.stop.setScaleType(ImageView.ScaleType.FIT_CENTER);
                    holder.stop.setVisibility(View.VISIBLE);

                    Resume();
                }

            }
        });
        holder.stop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                isclick = 0;

                SetScale(holder);
                Refresh();
            }
        });
        holder.pause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                isclick = 0;

                holder.resume.requestLayout();
                holder.resume.getLayoutParams().width = -2;
                holder.resume.setScaleType(ImageView.ScaleType.FIT_CENTER);

                holder.pause.setVisibility(View.INVISIBLE);
                holder.play.setVisibility(View.INVISIBLE);
                holder.stop.setVisibility(View.INVISIBLE);
                holder.resume.setVisibility(View.VISIBLE);

                popupMiss pm = new popupMiss();
                pm.id = Integer.valueOf(orderdetails9[position]);

                Date date = new Date();

                kayıpsaat = dateFormat.format(date).toString().trim();

                editor.putString("kayıpsaat", kayıpsaat);
                editor.commit();

                ctx.startActivity(new Intent(ctx, popupMiss.class));

            }
        });
        holder.annotation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                popupinfo pi = new popupinfo();
                pi.id = Integer.valueOf(orderdetails9[position]);

                ctx.startActivity(new Intent(ctx, popupinfo.class));



            }
        });

        mItemManger.bindView(holder.itemView, position);

    }

    private void Resume() {

        controlSwitcher=2;
        HttpAsyncTask miss=new HttpAsyncTask();
        miss.execute("http://10.10.1.46:8088/api/values/PostMissCont");
        //miss.execute("http://192.168.43.127:8088/api/values/PostMissCont");

    }

    private void Refresh() {


        controlSwitcher=3;
        HttpAsyncTask miss=new HttpAsyncTask();
        miss.execute("http://10.10.1.46:8088/api/values/Poststop");
        //miss.execute("http://192.168.43.127:8088/api/values/Poststop");


        ctx.startActivity(new Intent(ctx, DesignerActivity.class));

    }

    private void SetScale(MyViewHolder holder) {

        holder.play.requestLayout();
        holder.play.getLayoutParams().width = 200;
        holder.play.setScaleType(ImageView.ScaleType.FIT_CENTER);

        holder.pause.requestLayout();
        holder.pause.getLayoutParams().width = 200;
        holder.pause.setScaleType(ImageView.ScaleType.FIT_CENTER);

        holder.stop.requestLayout();
        holder.stop.getLayoutParams().width = 200;
        holder.stop.setScaleType(ImageView.ScaleType.FIT_CENTER);

        holder.resume.requestLayout();
        holder.resume.getLayoutParams().width = 200;
        holder.resume.setScaleType(ImageView.ScaleType.FIT_CENTER);

    }

    public void workplay() {
        controlSwitcher=0;
        HttpAsyncTask WorkPlay = new HttpAsyncTask();
        WorkPlay.execute("http://10.10.1.46:8088/api/values/Postplay");
        //WorkPlay.execute("http://192.168.43.127:8088/api/values/Postplay");
    }

    public static String POST(String url, JSONObject jsonObject) {
        InputStream inputStream = null;
        String result = "";
        try {

            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(url);
            String json = "";
            json = jsonObject.toString();
            StringEntity se = new StringEntity(json,"UTF-8");
            httpPost.setEntity(se);
            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("Content-type", "application/json");
            HttpResponse httpResponse = httpclient.execute(httpPost);
            inputStream = httpResponse.getEntity().getContent();
            if (inputStream != null)
                result = convertInputStreamToString(inputStream);
            else
                result = "Did not work!";

        } catch (Exception e) {
            Log.d("InputStream", e.getLocalizedMessage());
        }
        return result;
    }

    private class HttpAsyncTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {

            JSONObject jsonObject=new JSONObject();

            if(controlSwitcher==0){
                try {

                    jsonObject.put("token","a153dd6s33xv6uy9hgf23b16gh")
                            .put("id",wf.list_item_id);

                    Log.d("Jsonadapterplay",jsonObject.toString());

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
//            else if (controlSwitcher==1){
//                try {
//                    jsonObject.put("token","a153dd6s33xv6uy9hgf23b16gh")
//                            .put("id",wf.list_item_id)
//                            .put("kayip_id",pm.kayip_id);
//
//                    Log.d("Jsonadapterpause",jsonObject.toString());
//
//
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
            else if (controlSwitcher==2){
                try {

                    if(pm.kayip_id != "0"){
                        Date d = new Date();

                        jsonObject.put("token", "a153dd6s33xv6uy9hgf23b16gh")
                                .put("id", wf.list_item_id)
                                .put("kayip_id", pm.kayip_id)
                                .put("kayip_bas", preferences.getString("kayıpsaat", ""))
                                .put("kayip_son", dateFormat.format(d).toString().trim());

                        Log.d("Jsonadapterresumeif", jsonObject.toString());

                    }
                    else{
                    Date d = new Date();

                    jsonObject.put("token", "a153dd6s33xv6uy9hgf23b16gh")
                            .put("id", wf.list_item_id)
                            .put("kayip_id", Integer.valueOf(preferences.getString("kayip_id", "")))
                            .put("kayip_bas", preferences.getString("kayıpsaat", ""))
                            .put("kayip_son", dateFormat.format(d).toString().trim());

                    Log.d("Jsonadapterresumeelse", jsonObject.toString());

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
                catch(NumberFormatException e)
                {
                    Log.d("AdapterError","Programı kaldırmıs");
                }
                catch(RuntimeException e)
                {
                    Log.d("AdapterError","Programı kaldırmıs");
                }
            }
            else if (controlSwitcher==3){
                try {
                    jsonObject.put("token","a153dd6s33xv6uy9hgf23b16gh")
                            .put("id",wf.list_item_id);

                    Log.d("Jsonadapterstop",jsonObject.toString());


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }



            return POST(urls[0], jsonObject);
        }
        @Override
        protected void onPostExecute(String result) {



            if (controlSwitcher == 0) {

                Log.d("Jsonadapterplay", result);

                if (CheckConnect())
                {

                    if (Boolean.valueOf(result) == true) {
                    Toast.makeText(ctx, "İslem Baslatıldı.", Toast.LENGTH_SHORT).show();

                    //set(holder);

                    } else {
                    Toast.makeText(ctx.getApplicationContext(), " İslem Baslatma Hatası. ", Toast.LENGTH_SHORT).show();
                    }

                }
                else {
                    Toast.makeText(ctx.getApplicationContext(), "İnternet Baglantınızı Kontrol Edin.", Toast.LENGTH_SHORT).show();
                }
            }
            else if (controlSwitcher == 1) {

                Log.d("Jsonadapterpause", result);

                /*zaten spinner doldurulurken al kayıp id*/

                if (CheckConnect())
                {

                    if (Boolean.valueOf(result) == true) {
                        Toast.makeText(ctx, "İslem Durduruldu.", Toast.LENGTH_SHORT).show();

                        //set(holder);

                    } else {
                        Toast.makeText(ctx.getApplicationContext(), " İslem Durdurma Hatası. ", Toast.LENGTH_SHORT).show();
                    }

                }
                else {
                    Toast.makeText(ctx.getApplicationContext(), "İnternet Baglantınızı Kontrol Edin.", Toast.LENGTH_SHORT).show();
                }
            }
            else if (controlSwitcher == 2) {

                Log.d("JsonadapterresumePost", result);

                if (CheckConnect())
                {

                    if (Boolean.valueOf(result) == true) {
                        Toast.makeText(ctx, "İsleme Devam Ediliyor.", Toast.LENGTH_SHORT).show();

                        //set(holder);

                    } else {
                        Toast.makeText(ctx.getApplicationContext(), " İslem Devam Edilemedi. ", Toast.LENGTH_SHORT).show();
                    }

                }
                else {
                    Toast.makeText(ctx.getApplicationContext(), "İnternet Baglantınızı Kontrol Edin.", Toast.LENGTH_SHORT).show();
                }
            }
            else if (controlSwitcher == 3){

                Log.d("Jsonadapterstop", result);

                if (CheckConnect())
                {

                    if (Boolean.valueOf(result) == true) {
                        Toast.makeText(ctx, "İslem Bitirildi.", Toast.LENGTH_SHORT).show();

                        //set(holder);

                    } else {
                        Toast.makeText(ctx.getApplicationContext(), " İslem Bitirilemedi. ", Toast.LENGTH_SHORT).show();
                    }

                }
                else {
                    Toast.makeText(ctx.getApplicationContext(), "İnternet Baglantınızı Kontrol Edin.", Toast.LENGTH_SHORT).show();
                }

            }

            else {
                Toast.makeText(ctx.getApplicationContext(), "Connection Error 404", Toast.LENGTH_SHORT).show();
            }

        }
    }

    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while ((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;
    }

    public boolean CheckConnect() {

        ConnectivityManager conMgr = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);

        if (conMgr.getActiveNetworkInfo() != null && conMgr.getActiveNetworkInfo().isConnected() && conMgr.getActiveNetworkInfo().isConnected()) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int getItemCount() {
        return orderdetails1.length;
    }
}