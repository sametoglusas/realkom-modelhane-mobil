package com.ali.modelhane.Helper;

import android.content.Context;
import android.os.AsyncTask;
import android.renderscript.Sampler;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


public class RequestController extends AsyncTask<String,Void,String> {
    private Context context;
    private String mAction;
    private JSONObject postData;
    public RequestController(Context context,String action,Map<String, String> postData) {
        this.context = context;
        this.mAction = action;
        if (postData != null) {
            this.postData = new JSONObject(postData);
        }
    }

    @Override
    public void onPreExecute(){
        super.onPreExecute();
    }
    @Override
    public String doInBackground(String... params){

        HttpURLConnection connection = null;
        BufferedReader br = null;
        try{
            URL url = new URL(params[0]);
            connection = (HttpURLConnection) url.openConnection();
            connection.setReadTimeout(10000);
            connection.setConnectTimeout(15000);
            Log.d("ConnectionRequest","BasarılıRequest");


            if(mAction.equals("ghdtdh")){

                connection.connect();
                InputStream is = connection.getInputStream();
                br = new BufferedReader(new InputStreamReader(is));
                String line = "";
                String file = "";
                while((line = br.readLine()) != null){
                    Log.d("satirRequest",line);
                    file += line;
                }
                return file;
            }else if(mAction.equals("GetDeneme")){


                connection.setDoInput(true);
                connection.setDoOutput(true);
                connection.setRequestProperty("Content-Type", "application/json");
                connection.setRequestMethod("POST");

                if (this.postData != null) {
                    OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
                    writer.write(postData.toString());
                    writer.flush();

                    Log.d("DataRequest",postData.toString());
                    Log.d("mesajRequest","data işlendi");
                }

                int statusCode = connection.getResponseCode();
                if (statusCode ==  200) {
                    InputStream is = connection.getInputStream();
                    br = new BufferedReader(new InputStreamReader(is));
                    String line = "";
                    String file = "";
                    while((line = br.readLine()) != null){
                        Log.d("satirRequest",line);
                        file += line;
                    }
                    Log.d("responseRequest",file);
                    return file;
                    // From here you can convert the string to JSON with whatever JSON parser you like to use
                    // After converting the string to JSON, I call my custom callback. You can follow this process too, or you can implement the onPostExecute(Result) method
                } else {
                    // Status code is not 200
                    // Do something to handle the error
                    return "Connection Error";
                }
            }else if(mAction.equals("lang_list")){
                connection.setRequestProperty("Content-Type", "application/json");
                connection.setRequestMethod("POST");
                int statusCode = connection.getResponseCode();
                if (statusCode ==  200) {
                    InputStream is = connection.getInputStream();
                    br = new BufferedReader(new InputStreamReader(is));
                    String line = "";
                    String file = "";
                    while((line = br.readLine()) != null){
                        Log.d("satirRequest",line);
                        file += line;
                    }
                    Log.d("responseRequest",file);
                    return file;
                    // From here you can convert the string to JSON with whatever JSON parser you like to use
                    // After converting the string to JSON, I call my custom callback. You can follow this process too, or you can implement the onPostExecute(Result) method
                } else {
                    // Status code is not 200
                    // Do something to handle the error
                    return "Connection Error";
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return "hata";
    }

    @Override
    protected void onProgressUpdate(Void... values) {
        super.onProgressUpdate(values);
    }

    @Override
    public void onPostExecute(String s){
        super.onPostExecute(s);
        ReturnMethod(s);
    }

    private String ReturnMethod(String myValue) {
        return myValue;
    }
}
