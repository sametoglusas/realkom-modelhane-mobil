package com.ali.modelhane.Helper;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;

import java.util.ArrayList;


/**
 * Created by samet on 27.11.2017.
 */

public class ViewPagerAdapter extends FragmentPagerAdapter implements ViewPager.OnPageChangeListener {


    ArrayList<Fragment> fragments = new ArrayList<>();
    ArrayList<String>  tabtitles = new ArrayList<>();


    public void addFragments(Fragment fragments, String titles){

        this.fragments.add(fragments);
        this.tabtitles.add(titles);


    }
    public ViewPagerAdapter (FragmentManager fm)
    {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }

    @Override
    public int getCount() {
        return fragments.size();
    }


    @Override
    public CharSequence getPageTitle(int position) {
        return tabtitles.get(position);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {


    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
