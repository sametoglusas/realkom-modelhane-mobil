package com.ali.modelhane.Helper;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ali.modelhane.R;

public class ListAdapter extends RecyclerView.Adapter<ListAdapter.MyViewHolder> {


    public String [] orderdetails2 ;
    public String [] orderdetails3 ;
    public String [] orderdetails4 ;
    public String type_id ;

    public ListAdapter(String [] orderdetails2, String [] orderdetails3, String [] orderdetails4 , String type_id) {
        this.orderdetails2 = orderdetails2;
        this.orderdetails3 = orderdetails3;
        this.orderdetails4 = orderdetails4;
        this.type_id = type_id;

    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView lblod2,lblod3,lblod4,txt_info;


        MyViewHolder(View view) {
            super(view);
            lblod2 = view.findViewById(R.id.txt_order_owner);
            lblod3 = view.findViewById(R.id.txt_order_name);
            lblod4 = view.findViewById(R.id.txt_order_critic);

            txt_info = view.findViewById(R.id.txt_info);

            }
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.ordercellview, parent, false);
        return new MyViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        holder.lblod2.setText(orderdetails2[position]);
        holder.lblod3.setText(orderdetails3[position]);
        holder.lblod4.setText(orderdetails4[position]);

        if (type_id.contains("Devam")){
            holder.txt_info.setText("Modelist Değistirmek için Tıklayınız.");


        }
        else if (type_id.contains("Hazır")){
            holder.txt_info.setText("Bu sipariş Bitmiştir.");
        }
        else if (type_id.contains("Ask")){
            holder.txt_info.setText("Bilgi Eksikliğini Gidermek için Tıklayınız.");
        }

    }

    @Override
    public int getItemCount() {
        return orderdetails2.length;
    }
}
