package com.ali.modelhane.Main;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.Toast;

import com.ali.modelhane.Helper.Validation;
import com.ali.modelhane.Helper.pGS;
import com.ali.modelhane.R;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.client.HttpClient;


public class MainActivity extends AppCompatActivity {

    Button btnSignin;

    TextInputLayout etl_name,etl_pass;

    public ProgressBar progressBar;

    public ScrollView scrollView;
    public pGS pgs=new pGS(false);
    Boolean isbegin = false;


    public static String kullanici_id;
    public static Boolean isAdmin=false;


    Validation validationCheck = new Validation();
    private static final int REQUEST_PERMISSIONS = 0;


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_PERMISSIONS: {
                for (int i = 0; i < permissions.length; i++) {
                    if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                        Log.d("Permissions", "İzin Verildi: " + permissions[i]);

                    } else if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                        Log.d("Permissions", "İzin Reddedildi: " + permissions[i]);
                    }
                }
            }
            break;
            default: {
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            }
        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        permission();

        if (CheckConnect()) {
            SignIn();
        } else {
            Toast.makeText(MainActivity.this, "Lütfen İnternet Bağlantınızı Kontrol Ederek Uygulamayı Tekrar Başlatın.", Toast.LENGTH_LONG).show();
        }



    }

    public void SignIn() {

        setTheme(R.style.MyMaterialTheme);
        setContentView(R.layout.activity_main);

        scrollView=(ScrollView)findViewById(R.id.login_form);
        progressBar = (ProgressBar) findViewById(R.id.progressBar_cyclic);
        progressBar.setVisibility(View.INVISIBLE);



        /**/
        etl_name = findViewById(R.id.etl_name);
        etl_pass = findViewById(R.id.etl_pass);
        /**/

        btnSignin = (Button) findViewById(R.id.btn_sign);

        btnSignin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signin();


            }
        });

    }

    public void startPGS(){
        scrollView.setVisibility(View.INVISIBLE);
        progressBar.setVisibility(View.VISIBLE);
        pgs.start();
        isbegin = true;
    }
    public void stopPGS(){
        scrollView.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.INVISIBLE);
        //pgs.stop();
    }

    public void signin() {

        int errorCount=1;


        if (!validationCheck.isTcCorrect(etl_name.getEditText().getText().toString().trim()))
            {
                errorCount=0;
                etl_name.setError("Geçersiz Tc no");
            }


            if (validationCheck.isPassEmpty(etl_pass.getEditText().getText().toString().trim()))
            {
                errorCount=0;
                etl_pass.setError("Şifre Boş Bırakılamaz");
            }

            if(errorCount==1) {
                if (isbegin) {

                    stopPGS();
                    pgs.setGoster(true);
                    pgs.setGoster(false);

                    HttpAsyncTask LogIn = new HttpAsyncTask();
                    //LogIn.execute("http://192.168.43.127:8088/api/values/UserCheck");
                    LogIn.execute("http://10.10.1.46:8088/api/values/UserCheck");
                    //LogIn.execute("http://192.168.10.73:8088/api/values/UserCheck");

                } else {


                    stopPGS();
                    pgs.setGoster(false);
                    pgs.setGoster(true);
                    startPGS();

                    HttpAsyncTask LogIn = new HttpAsyncTask();
                    //LogIn.execute("http://192.168.43.127:8088/api/values/UserCheck");
                    LogIn.execute("http://10.10.1.46:8088/api/values/UserCheck");
                    //LogIn.execute("http://192.168.10.73:8088/api/values/UserCheck");

//                Intent i = new Intent(getApplicationContext(), ListActivity.class);
//                startActivity(i);
                }
            }

    }

    public static String POST(String url, JSONObject jsonObject) {
        InputStream inputStream = null;
        String result = "";
        try {
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(url);
            String json = "";
            json = jsonObject.toString();
            StringEntity se = new StringEntity(json, "UTF-8");
            httpPost.setEntity(se);
            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("Content-type", "application/json");
            HttpResponse httpResponse = httpclient.execute(httpPost);
            inputStream = httpResponse.getEntity().getContent();
            if (inputStream != null)
                result = convertInputStreamToString(inputStream);
            else
                result = "Did not work!";

        } catch (Exception e) {
            Log.d("InputStream", e.getLocalizedMessage());
        }
        return result;
    }


    private class HttpAsyncTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {

            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("token", "a153dd6s33xv6uy9hgf23b16gh")
                        .put("tc", etl_name.getEditText().getText().toString().trim())
                        .put("pass", etl_pass.getEditText().getText().toString().trim());

            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.d("JsonUser",jsonObject.toString());
            return POST(urls[0], jsonObject);

        }


        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {

            Log.d("JsonResponseMain", result);

            if(result.contains("Hata"))
            {
                Toast.makeText(MainActivity.this, "Tc Kimlik numaranızı ve Sifrenizi Dogru Girdiğinizden Emin olun !!!", Toast.LENGTH_SHORT).show();
                Log.d("burada", "burada");

                stopPGS();
                pgs.setGoster(false);
                pgs.setGoster(true);


            }
            else {

            try {

//                Toast.makeText(MainActivity.this, result, Toast.LENGTH_SHORT).show();
                Log.d("JsonResponseMain", result);

                try {

                    JSONObject jsonObject = new JSONObject(new String(result.getBytes("ISO-8859-1"), "UTF-8"));
                    kullanici_id = jsonObject.getString("user_id");
                    isAdmin = jsonObject.getBoolean("isAdmin");
                    /**/
//                    isAdmin = false;
                    /**/
                    if (isAdmin == true) {
//                        startPGS();

                        Intent i = new Intent(getApplicationContext(), ListActivity.class);
                        startActivity(i);
                    } else {
                        Intent i = new Intent(getApplicationContext(), DesignerActivity.class);
                        startActivity(i);
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.d("burada","burada2");

                    Toast.makeText(MainActivity.this, "Tc Kimlik numaranızı ve Sifrenizi Dogru Girdiğinizden Emin olun !!!", Toast.LENGTH_SHORT).show();
                    stopPGS();

                    pgs.setGoster(true);

                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    //stopPGS();

                }
            }
            catch (Exception ex)
            {
//                Toast.makeText(MainActivity.this, "Connection Error1", Toast.LENGTH_SHORT).show();
                //stopPGS();
                Log.d("burada","burada3");


            }

            }
        }


    }


    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while ((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;
    }


    public void permission() {
        int internetizni = ContextCompat.checkSelfPermission(this, Manifest.permission.INTERNET);
        int mobilveriizni = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_NETWORK_STATE);
        int wifiizni = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_WIFI_STATE);

        List<String> izinler = new ArrayList<String>();

        if (internetizni != PackageManager.PERMISSION_GRANTED) {

            izinler.add(Manifest.permission.INTERNET);
        }
        if (mobilveriizni != PackageManager.PERMISSION_GRANTED) {

            izinler.add(Manifest.permission.ACCESS_NETWORK_STATE);
        }
        if (wifiizni != PackageManager.PERMISSION_GRANTED) {

            izinler.add(Manifest.permission.ACCESS_WIFI_STATE);
        }

        if (!izinler.isEmpty()) {

            ActivityCompat.requestPermissions(this,
                    izinler.toArray(new String[izinler.size()]), REQUEST_PERMISSIONS);
        }
    }

    public boolean CheckConnect() {

        ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        if (conMgr.getActiveNetworkInfo() != null && conMgr.getActiveNetworkInfo().isConnected() && conMgr.getActiveNetworkInfo().isConnected()) {
            return true;
        } else {
            return false;
        }
    }



}


