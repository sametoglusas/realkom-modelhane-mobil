package com.ali.modelhane.Main;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Toast;

import com.ali.modelhane.Helper.ListPopupAdapter;
import com.ali.modelhane.Helper.pGS;
import com.ali.modelhane.R;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

public class popup extends AppCompatActivity {

    RecyclerView listView;
    ListPopupAdapter adapter;

    public ProgressBar pb_degree,pb_fullness;


    ListDesigner ld = new ListDesigner();



    public static String [] ids;

    public static String [] od_2;
    public static String [] od_3;
    public static String [] od_4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_popup);


        listView = findViewById(R.id.list_orders_designer);
        pb_degree = findViewById(R.id.pb_degree);
        pb_fullness = findViewById(R.id.pb_fullness);


        pb_degree.setProgress(ld.degree);
        pb_fullness.setProgress(ld.Percent_of_fullness);


        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = dm.widthPixels;
        int height = dm.heightPixels;

        getWindow().setLayout((int)(width*.8),(int)(height*.6));


        ListOrders();



    }




    public void ilanlistele() {

        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT);
        LinearLayoutManager llm = new LinearLayoutManager(this.getApplicationContext());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        listView.setLayoutManager(llm);
        listView.setLayoutParams(params);
        listView.setHasFixedSize(true);

        adapter = new ListPopupAdapter(od_2,od_3,od_4);
        listView.setAdapter(adapter);

        listView.addItemDecoration(new DividerItemDecoration(this.getApplicationContext(), LinearLayoutManager.VERTICAL));

    }

    public void ListOrders() {
        HttpAsyncTask listAllAnno = new HttpAsyncTask();
        //listAllAnno.execute("http://192.168.10.73:8088/api/values/AllOrderDesignerPostByAdmin");
        listAllAnno.execute("http://10.10.1.46:8088/api/values/AllOrderDesignerPostByAdmin");
        //listAllAnno.execute("http://192.168.43.127:8088/api/values/AllOrderDesignerPostByAdmin");
    }

    public static String POST(String url, JSONObject jsonObject) {
        InputStream inputStream = null;
        String result = "";
        try {

            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(url);
            String json = "";
            json = jsonObject.toString();
            StringEntity se = new StringEntity(json,"UTF-8");
            httpPost.setEntity(se);
            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("Content-type", "application/json");
            HttpResponse httpResponse = httpclient.execute(httpPost);
            inputStream = httpResponse.getEntity().getContent();
            if (inputStream != null)
                result = convertInputStreamToString(inputStream);
            else
                result = "Did not work!";

        } catch (Exception e) {
            Log.d("InputStream", e.getLocalizedMessage());
        }
        return result;
    }

    private class HttpAsyncTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {

            JSONObject jsonObject = new JSONObject();


            try {

                jsonObject.put("token", "a153dd6s33xv6uy9hgf23b16gh")
                        .put("id", ld.list_designer_id);

                Log.d("Jsonpopid",jsonObject.toString());


            } catch (JSONException e) {
                e.printStackTrace();
            }


            return POST(urls[0], jsonObject);
        }

        @Override
        protected void onPostExecute(String result) {

            try {
                Log.d("Jsonpopresult",result);


                JSONObject jsonObject = new JSONObject(new String(result.getBytes("ISO-8859-1"), "UTF-8"));


                JSONArray jsonArray = new JSONArray(jsonObject.getString("results"));

                try {
                    String[] od2 = new String[jsonArray.length()];
                    String[] od3 = new String[jsonArray.length()];
                    String[] od4 = new String[jsonArray.length()];


                        Log.d("Jsonpopresultarray", jsonArray.toString());


                        if (jsonArray.length() > 0) {
                            String[] idler = new String[jsonArray.length()];


                            for (int i = 0; i < jsonArray.length(); i++) {

                                JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                                Log.d("JsonBurada", jsonObject1.toString());

                                od2[i] = jsonObject1.getString("model_adi").toString();
                                od3[i] = jsonObject1.getString("baslangic_tarih").toString();
                                od4[i] = jsonObject1.getString("isinsuresi").toString();


                                idler[i] = jsonObject1.getString("siparis_id");

                            }



                            od_2 = od2;
                            od_3 = od3;
                            od_4 = od4;
                            ids = idler;

                            ilanlistele();
                        } else {
                            Toast.makeText(popup.this, "Modeliste atanmıs Sipariş Yok", Toast.LENGTH_SHORT).show();
                        }

                } catch (Exception ex) {
                    Toast.makeText(popup.this, "Baglantınızı Kontrol Edin", Toast.LENGTH_SHORT).show();
                }

            } catch (JSONException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
    }


    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while ((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;
    }

}
