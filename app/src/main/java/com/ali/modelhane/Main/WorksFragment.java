package com.ali.modelhane.Main;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.ali.modelhane.Helper.DividerItemDecoration;
import com.ali.modelhane.Helper.ListWorksAdapter;
import com.ali.modelhane.Helper.RecyclerItemClickListener;
import com.ali.modelhane.R;
import com.daimajia.swipe.SwipeLayout;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.Random;

import jp.wasabeef.recyclerview.animators.FadeInLeftAnimator;


public class WorksFragment extends Fragment {

    RecyclerView listView;
    ListWorksAdapter adapter;
    public static String type_id="Yeni";
    public static String [] ids;
    public static int controlSwitcher = 0;


    SwipeLayout swipeLayout;
    İtem item=new İtem();

    public static String [] od_1;
    public static String [] od_2;
    public static String [] od_3;
    public static String [] od_4;
    public static String [] od_5;
    public static String [] od_6;
    public static String [] od_7;
    public static String [] od_8;
    public static String [] od_9;



    public static int list_item_id=0;

    MainActivity ma = new MainActivity();


    public WorksFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_works, container, false);


        listView = (RecyclerView)view.findViewById(R.id.list_works);



        ListOrders();
//        listorders();

        listView.addOnItemTouchListener(
                new RecyclerItemClickListener(this.getContext(), listView ,new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        list_item_id = Integer.valueOf(String.valueOf(ids[position]));
                    }
                    @Override
                    public void onLongItemClick(View view, int position) {
                    }
                })
        );

        return view;

    }

    public void ilanlistele() {

        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT);
        LinearLayoutManager llm = new LinearLayoutManager(this.getContext());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        listView.setLayoutManager(llm);


        listView.setLayoutParams(params);
        listView.setHasFixedSize(true);

        adapter = new ListWorksAdapter(this.getContext(),od_1,od_2,od_3,od_4,od_5,od_6,od_7,od_8,od_9);
        listView.setAdapter(adapter);

        listView.setLayoutManager(new LinearLayoutManager(this.getContext()));

        listView.addItemDecoration(new DividerItemDecoration(getResources().getDrawable(R.drawable.divider)));
        listView.setItemAnimator(new FadeInLeftAnimator());


    }

    public void ListOrders() {
        controlSwitcher=1;
        HttpAsyncTask listAllAnno = new HttpAsyncTask();
//        listAllAnno.execute("http://192.168.43.127:8088/api/values/AllOrderPostByDesigner");

        //listAllAnno.execute("http://192.168.10.73:8088/api/values/AllOrderPostByDesigner");
        listAllAnno.execute("http://10.10.1.46:8088/api/values/AllOrderPostByDesigner");

    }


    public void TakeAnno(){
        controlSwitcher = 2;
        HttpAsyncTask takeAllAnno = new HttpAsyncTask();
        takeAllAnno.execute("web_api_link");
    }


    public static String POST(String url, JSONObject jsonObject) {
        InputStream inputStream = null;
        String result = "";
        try {

            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(url);
            String json = "";
            json = jsonObject.toString();
            StringEntity se = new StringEntity(json,"UTF-8");
            httpPost.setEntity(se);
            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("Content-type", "application/json");
            HttpResponse httpResponse = httpclient.execute(httpPost);
            inputStream = httpResponse.getEntity().getContent();
            if (inputStream != null)
                result = convertInputStreamToString(inputStream);
            else
                result = "Did not work!";

        } catch (Exception e) {
            Log.d("InputStream", e.getLocalizedMessage());
        }
        return result;
    }

    private class HttpAsyncTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {

            JSONObject jsonObject=new JSONObject();

            if(controlSwitcher==1){
                try {
                    jsonObject.put("token","a153dd6s33xv6uy9hgf23b16gh")
                              .put("id",ma.kullanici_id);

                    Log.d("JsonDesignerOrder",jsonObject.toString());

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            else if (controlSwitcher==2){
                try {
                    jsonObject.put("token","a153dd6s33xv6uy9hgf23b16gh")
                            .put("work_id",list_item_id);

                    Log.d("JsonDesignerOrderOne",jsonObject.toString());


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            return POST(urls[0], jsonObject);
        }
        @Override
        protected void onPostExecute(String result) {

            if(controlSwitcher==1){

                Log.d("JsonDesignerresult",result);

                try {


                    JSONObject jsonObject = new JSONObject(new String(result.getBytes("ISO-8859-1"), "UTF-8"));


                    JSONArray jsonArray = new JSONArray(jsonObject.getString("results"));

                    try {
                        String[] od1 = new String[jsonArray.length()];
                        String[] od2 = new String[jsonArray.length()];
                        String[] od3 = new String[jsonArray.length()];
                        String[] od4 = new String[jsonArray.length()];
                        String[] od5 = new String[jsonArray.length()];
                        String[] od6 = new String[jsonArray.length()];
                        String[] od7 = new String[jsonArray.length()];
                        String[] od8 = new String[jsonArray.length()];
                        String[] od9 = new String[jsonArray.length()];



                        if (jsonArray.length() > 0) {
                            String[] idler = new String[jsonArray.length()];
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                                od1[i] = jsonObject1.getString("kritik").toString();
                                od2[i] = jsonObject1.getString("isintanimi").toString();
                                od3[i] = jsonObject1.getString("urun").toString();
                                od4[i] = jsonObject1.getString("zorluk").toString();
                                od5[i] = jsonObject1.getString("model_name").toString();
                                od6[i] = jsonObject1.getString("order_no").toString();
                                od7[i] = jsonObject1.getString("model_no").toString();
                                od8[i] = jsonObject1.getString("isemri_durum").toString();
                                od9[i] = jsonObject1.getString("calisma_id").toString();


                                idler[i] = jsonObject1.getString("calisma_id");

                            }
                            od_1 = od1;
                            od_2 = od2;
                            od_3 = od3;
                            od_4 = od4;
                            od_5 = od5;
                            od_6 = od6;
                            od_7 = od7;
                            od_8 = od8;
                            od_9 = od9;

                            ilanlistele();
                            ids = idler;
                        } else {
                            Toast.makeText(WorksFragment.this.getContext(), "İş Yok", Toast.LENGTH_SHORT).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(WorksFragment.this.getContext(), "Connection Error 404", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
            else if(controlSwitcher==2){

                try {
                    JSONArray jsonArray=new JSONArray(new String(result.getBytes("ISO-8859-1"), "UTF-8"));
                    JSONObject jsonObject = jsonArray.getJSONObject(0);

                    JSONArray jsonArrayworkDetails=new JSONArray(jsonObject.getString("mobile_work_details"));
                    JSONObject jsonObjectworkDetails=jsonArrayworkDetails.getJSONObject(0);

                    item.txt_number = (jsonObjectworkDetails.getString("customer").toString());
                    item.txt_owner = (jsonObjectworkDetails.getString("name").toString());
                    item.txt_type = (jsonObjectworkDetails.getString("no").toString());


                    /*Yeni activity de item sayfasına gider*/
                    Intent i = new Intent(WorksFragment.this.getContext(), İtem.class);
                    startActivity(i);

                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(WorksFragment.this.getContext(), "Connection Error3", Toast.LENGTH_SHORT).show();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }

            else{
                Toast.makeText(WorksFragment.this.getContext(), "Connection Error 2", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while ((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;
    }



}
