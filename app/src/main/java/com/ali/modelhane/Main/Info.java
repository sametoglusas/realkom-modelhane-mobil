package com.ali.modelhane.Main;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Toast;

import com.ali.modelhane.R;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

public class Info extends AppCompatActivity {

    //public  EditText et_model_no,et_model_name,et_order_no,et_aciklama;
    public TextInputLayout etl_model_no,etl_model_name,etl_order_no,etl_aciklama;
    /**/
    public  Spinner list_cins, list_tur,list_once;

    RadioButton rb_kolay, rb_orta, rb_zor;
    Button btn_edit;

    public String []cins;
    public String []urunler;

    public static String model_no,model_name,order_no,aciklama;

    public static int controlSwitcheritem;

    public static int Oncelik=1;
    public static String isemri ="";
    public static String urun ="";
    public static String zorluk = "";


    InfosFragment ıf = new InfosFragment();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);
    /*
        et_aciklama = findViewById(R.id.et_aciklama);
        et_order_no = findViewById(R.id.et_order_no);
        et_model_name = findViewById(R.id.et_model_name);
        et_model_no = findViewById(R.id.et_model_no);

        et_aciklama.setText(aciklama);
        et_order_no.setText(order_no);
        et_model_name.setText(model_name);
        et_model_no.setText(model_no);

    */

        etl_aciklama = findViewById(R.id.etl_aciklama);
        etl_model_name = findViewById(R.id.etl_model_name);
        etl_model_no = findViewById(R.id.etl_model_no);
        etl_order_no = findViewById(R.id.etl_order_no);

        etl_aciklama.setHint("Aciklama");
        etl_order_no.setHint("Order No");
        etl_model_name.setHint("Model Adi");
        etl_model_no.setHint("Model No");

        etl_model_name.getEditText().setText(model_name);
        etl_model_no.getEditText().setText(model_no);
        etl_order_no.getEditText().setText(order_no);
        etl_aciklama.getEditText().setText(aciklama);

        /**/


        btn_edit = findViewById(R.id.btn_edit);

        list_cins = findViewById(R.id.list_cins);
        list_tur = findViewById(R.id.list_tur);
        list_once = findViewById(R.id.list_oncelik);

        rb_kolay = findViewById(R.id.rb_kolay);
        rb_orta = findViewById(R.id.rb_orta);
        rb_zor = findViewById(R.id.rb_zor);

        cinsAl();

        list_cins.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(i!=0)
                {
                    isemri = list_cins.getItemAtPosition(i).toString();

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });



        /**/

        list_tur.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int i, long id) {
                urun = list_tur.getItemAtPosition(i).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });



        list_once.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Oncelik = Integer.valueOf(list_once.getItemAtPosition(position).toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        rb_kolay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rb_kolay.setChecked(true);
                rb_orta.setChecked(false);
                rb_zor.setChecked(false);
                zorluk = "Kolay";
            }
        });

        rb_orta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rb_kolay.setChecked(false);
                rb_orta.setChecked(true);
                rb_zor.setChecked(false);
                zorluk = "Orta";
            }
        });

        rb_zor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rb_kolay.setChecked(false);
                rb_orta.setChecked(false);
                rb_zor.setChecked(true);
                zorluk = "Zor";
            }
        });

        switch (zorluk) {
            case "Kolay" :
                rb_kolay.setChecked(true);
                zorluk = "Kolay";

                break;

            case "Orta" :
                rb_orta.setChecked(true);
                zorluk = "Orta";

                break;
            case "Zor" :
                rb_zor.setChecked(true);
                zorluk = "Zor";

                break;

            default :
                rb_kolay.setChecked(true);
                zorluk = "Kolay";

                break;
        }





        btn_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                controlSwitcheritem = 1;

                Info.HttpAsyncTask Info=new Info.HttpAsyncTask();
//                Info.execute("http://192.168.43.127:8088/api/values/PostInfo");
                Info.execute("http://10.10.1.46:8088/api/values/PostInfo");
                //Info.execute("http://192.168.10.73:8088/api/values/PostInfo");


//                Intent i = new Intent(İtem.this, ListDesigner.class);
//                startActivity(i);

            }
        });

    }

    public void cinsAl() {
        controlSwitcheritem=2;
        Info.HttpAsyncTask CinsAl=new Info.HttpAsyncTask();
        //CinsAl.execute("http://192.168.10.73:8088/api/values/GetWork");
        CinsAl.execute("http://10.10.1.46:8088/api/values/GetWork");
        //CinsAl.execute("http://192.168.43.127:8088/api/values/GetWork");
    }
    public void urunAl() {
        controlSwitcheritem=3;
        Info.HttpAsyncTask UrunAl=new Info.HttpAsyncTask();
        //CinsAl.execute("http://192.168.43.127:8088/api/values/GetWork");
        UrunAl.execute("http://10.10.1.46:8088/api/values/GetProduct");
        //CinsAl.execute("http://192.168.10.73:8088/api/values/GetWork");
    }



    public static String POST(String url, JSONObject jsonObject) {
        InputStream inputStream = null;
        String result = "";
        try {

            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(url);
            String json = "";
            json = jsonObject.toString();
            StringEntity se = new StringEntity(json,"UTF-8");
            httpPost.setEntity(se);
            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("Content-type", "application/json");
            HttpResponse httpResponse = httpclient.execute(httpPost);
            inputStream = httpResponse.getEntity().getContent();
            if (inputStream != null)
                result = convertInputStreamToString(inputStream);
            else
                result = "Did not work!";

        } catch (Exception e) {
            Log.d("InputStream", e.getLocalizedMessage());
        }
        return result;
    }

    private class HttpAsyncTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {

            JSONObject jsonObject = new JSONObject();

            if (controlSwitcheritem == 1)
            {
                try {
                    jsonObject.put("token", "a153dd6s33xv6uy9hgf23b16gh")
                            .put("id", ıf.list_item_id)
                            .put("isemri", list_cins.getSelectedItem().toString())
                            .put("urun", list_tur.getSelectedItem().toString())
                            .put("zorluk", zorluk)
                            .put("oncelik", Oncelik);

                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(Info.this, "Connection Error", Toast.LENGTH_SHORT).show();
                }

                Log.d("JsonİnfoPost", jsonObject.toString());

            }
            else if (controlSwitcheritem == 2){
                try {
                    jsonObject.put("token", "a153dd6s33xv6uy9hgf23b16gh");
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(Info.this, "İsemri Error", Toast.LENGTH_SHORT).show();
                }

                Log.d("JsonİsemriPost", jsonObject.toString());

            }

            else if (controlSwitcheritem == 3){
                try {
                    jsonObject.put("token", "a153dd6s33xv6uy9hgf23b16gh");
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(Info.this, "Ürünler Alınamadı", Toast.LENGTH_SHORT).show();
                }

                Log.d("JsonUrunPost", jsonObject.toString());

            }

            else {

                Toast.makeText(Info.this, "Post Error", Toast.LENGTH_SHORT).show();

            }



            return POST(urls[0], jsonObject);
        }

        @Override
        protected void onPostExecute(String result) {

            if(controlSwitcheritem == 1) {


                Log.d("JsonİsemriPostResult", result.toString());

                if (Boolean.valueOf(result) == true)
                {
                    Toast.makeText(getApplicationContext(), "Bilgi Eksiği Giderildi.", Toast.LENGTH_SHORT).show();

                    /*buraya hem designerın hem işin id si gonderilsin sonra da result donsun  belki iş idsine gerek kalmaz*/

                    Intent i = new Intent(getApplicationContext(), ListActivity.class);
                    startActivity(i);

                } else {
                    Toast.makeText(getApplicationContext(), "İs Emrini Dogru Sectiginizden Emin olun !", Toast.LENGTH_SHORT).show();
                }



            }
            else if(controlSwitcheritem == 2) {
                try {

                    Log.d("Jsonİnfoİsemri", result);

                    JSONObject jsonObject = new JSONObject(new String(result.getBytes("ISO-8859-1"), "UTF-8"));

                    JSONArray jsonArray = new JSONArray(jsonObject.getString("results"));

                    String İsemri_ıd[]=new String[jsonArray.length()];
                    ArrayList İsemri=new ArrayList<>();
                    İsemri.add("");
                    for (int i=0;i<jsonArray.length();i++){
                        JSONObject jsonObject1=jsonArray.getJSONObject(i);
                        İsemri_ıd[i]=jsonObject1.get("id").toString();
                        İsemri.add(jsonObject1.get("name").toString());
                    }
                    cins=İsemri_ıd;
                    ArrayAdapter<String> adCinsler= new ArrayAdapter<String>(Info.this,android.R.layout.simple_spinner_dropdown_item,İsemri);
                    list_cins.setAdapter(adCinsler);


                    urunAl();


                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(Info.this, "İsemrinde Bir Hata Oluştu", Toast.LENGTH_SHORT).show();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }

            else if (controlSwitcheritem == 3) {
                try {

                    Log.d("JsonİnfoUrun", result);

                    JSONObject jsonObject = new JSONObject(new String(result.getBytes("ISO-8859-1"), "UTF-8"));

                    JSONArray jsonArray = new JSONArray(jsonObject.getString("results"));

                    String urun_id[]=new String[jsonArray.length()];
                    ArrayList Urun=new ArrayList<>();
                    Urun.add("");
                    for (int i=0;i<jsonArray.length();i++){
                        JSONObject jsonObject1=jsonArray.getJSONObject(i);
                        urun_id[i]=jsonObject1.get("id").toString();
                        Urun.add(jsonObject1.get("name").toString());
                    }
                    urunler=urun_id;
                    ArrayAdapter <String> adUrunler= new ArrayAdapter<String>(Info.this,android.R.layout.simple_spinner_dropdown_item,Urun);
                    list_tur.setAdapter(adUrunler);


                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(Info.this, "Üründe Bir Hata Oluştu", Toast.LENGTH_SHORT).show();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }

            else
                Toast.makeText(Info.this, "Connection Error", Toast.LENGTH_SHORT).show();
        }
    }

    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while ((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;
    }

//    @Override
//    public void onBackPressed() {
//
//    }
}
