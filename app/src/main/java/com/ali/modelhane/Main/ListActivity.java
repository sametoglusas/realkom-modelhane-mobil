package com.ali.modelhane.Main;

import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.ali.modelhane.Helper.ViewPagerAdapter;
import com.ali.modelhane.R;

public class ListActivity extends AppCompatActivity {

    TabLayout tabLayout;
    ViewPager viewPager;
    ViewPagerAdapter viewPagerAdapter;
    int IDviewPager=0;
    MainActivity ma = new MainActivity();

    /*arama*/
    EditText txt_ara;
    ImageView img_ara;
    /**/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        tabLayout =(TabLayout)findViewById(R.id.tabLayout);
        viewPager =(ViewPager)findViewById(R.id.viewPager);

        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPagerAdapter.addFragments(new OrdersFragment(),"Siparisler");
        viewPagerAdapter.addFragments(new ContinuesFragment(),"Devam eden Siparisler");
        viewPagerAdapter.addFragments(new FinishsFragment(),"Biten Siparisler");
        viewPagerAdapter.addFragments(new InfosFragment(),"Eksiği olan Siparisler");

        viewPager.setAdapter(viewPagerAdapter);
        tabLayout.setupWithViewPager(viewPager);

//        img_ara = findViewById(R.id.img_ara);
//        txt_ara = findViewById(R.id.txt_ara);
//
//        img_ara.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
////                String ara = txt_ara.getText().toString().trim();
////                OrdersFragment of = new OrdersFragment();
////                of.ListOrdersByAra(ara);
//            }
//        });



        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {
                IDviewPager=viewPager.getCurrentItem();

                if(IDviewPager==0)
                {
                    OrdersFragment.type_id="Yeni";
                }
                if(IDviewPager==1)
                {
                    ContinuesFragment.type_id="Devam";
                }
                if(IDviewPager==2)
                {
                    FinishsFragment.type_id="Hazır";
                }
                if(IDviewPager==3)
                {
                    InfosFragment.type_id="Devam";
                }
            }
        });
    }
    @Override
    public void onBackPressed() {

    }
}
