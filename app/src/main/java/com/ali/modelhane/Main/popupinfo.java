package com.ali.modelhane.Main;

import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.LabeledIntent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.AsyncTask;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.ali.modelhane.R;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

public class popupinfo extends AppCompatActivity {

//    EditText et_info;
    TextInputLayout etl_info;

    Button btn_Ok;

    public static int id = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_popupinfo);

        btn_Ok  = findViewById(R.id.btn_Ok);
        /**/
        //et_info = findViewById(R.id.et_info);

        etl_info = findViewById(R.id.etl_info);
        etl_info.setHint("Aciklama");
        /**/

        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = dm.widthPixels;
        int height = dm.heightPixels;

        getWindow().setLayout((int)(width*.8),(int)(height*.4));


        etl_info.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if(s.length() != 0)
                    btn_Ok.setEnabled(true);
            }
        });


        btn_Ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GetInfo();
                //PostEmail();
                finish();
            }
        });
    }


    public void GetInfo() {
        popupinfo.HttpAsyncTask ınfo=new popupinfo.HttpAsyncTask();
        ınfo.execute("http://10.10.1.46:8088/api/values/Postinfoorder");
        //ınfo.execute("http://192.168.10.73:8088/api/values/Postinfoorder");
        //ınfo.execute("http://192.168.43.127:8088/api/values/Postinfoorder");
    }
    public void PostEmail() {
        PackageManager pm = getPackageManager();
        Intent webIntent = new Intent(Intent.ACTION_SEND);
        webIntent.setType("text/plain");

        Intent openInChooser = Intent.createChooser(webIntent, "");


        List<ResolveInfo> resInfo = pm.queryIntentActivities(webIntent, 0);
        List<LabeledIntent> intentList = new ArrayList<LabeledIntent>();
        for (int i = 0; i < resInfo.size(); i++) {
            // Extract the label, append it, and repackage it in a LabeledIntent
            ResolveInfo ri = resInfo.get(i);
            String packageName = ri.activityInfo.packageName;
            if(packageName.contains("android.email")) {
                webIntent.setPackage(packageName);
            } else if(packageName.contains("android.gm")) {
                Intent intent = new Intent();
                intent.setComponent(new ComponentName(packageName, ri.activityInfo.name));
                intent.setAction(Intent.ACTION_SEND);
                intent.setType("text/plain");
                    intent.putExtra(Intent.EXTRA_TEXT, Html.fromHtml("sametoglu-sas@hotmail.com"));
                    intent.putExtra(Intent.EXTRA_SUBJECT, "Bilgi Eksikliği");
                    intent.setType("message/rfc822");

                intentList.add(new LabeledIntent(intent, packageName, ri.loadLabel(pm), ri.icon));
            }
        }

        // convert intentList to array
        LabeledIntent[] extraIntents = intentList.toArray( new LabeledIntent[ intentList.size() ]);

        openInChooser.putExtra(Intent.EXTRA_INITIAL_INTENTS, extraIntents);
        startActivity(openInChooser);



    }

    public static String POST(String url, JSONObject jsonObject) {
        InputStream inputStream = null;
        String result = "";
        try {

            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(url);
            String json = "";
            json = jsonObject.toString();
            StringEntity se = new StringEntity(json,"UTF-8");
            httpPost.setEntity(se);
            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("Content-type", "application/json");
            HttpResponse httpResponse = httpclient.execute(httpPost);
            inputStream = httpResponse.getEntity().getContent();
            if (inputStream != null)
                result = convertInputStreamToString(inputStream);
            else
                result = "Did not work!";

        } catch (Exception e) {
            Log.d("InputStream", e.getLocalizedMessage());
        }
        return result;
    }

    private class HttpAsyncTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {

            JSONObject jsonObject = new JSONObject();

            try {

                jsonObject.put("token", "a153dd6s33xv6uy9hgf23b16gh")
                        .put("id",id )
                        .put("neden", etl_info.getEditText().getText().toString().trim());

                Log.d("Jsonpopinfo",jsonObject.toString());


            } catch (JSONException e) {
                e.printStackTrace();
            }


            return POST(urls[0], jsonObject);
        }

        @Override
        protected void onPostExecute(String result) {



                Log.d("JsonAtama", result.toString());


                if (Boolean.valueOf(result) == true)
                {
                    Toast.makeText(getApplicationContext(), "Yoneticiye Bilgi Eksiği Teslim Edildi.", Toast.LENGTH_SHORT).show();

                    Intent i = new Intent(popupinfo.this, DesignerActivity.class);
                    startActivity(i);

                } else {
                    Toast.makeText(getApplicationContext(), "Eksik Hatası", Toast.LENGTH_SHORT).show();
                }



        }
    }


    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while ((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;
    }


    @Override
    public void onBackPressed() {

    }
}
