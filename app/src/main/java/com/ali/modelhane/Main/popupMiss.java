package com.ali.modelhane.Main;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.ali.modelhane.Helper.ListWorksAdapter;
import com.ali.modelhane.R;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class popupMiss extends AppCompatActivity {


    Spinner list_miss;

    public static String kayip_id = "0" ;
    public static String kayip_saat = "0";

    public static String [] ids;

    public static int controlSwitcheritem;

    Button btn_Ok;

    public static int id = 0;

    SharedPreferences preferences;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_popupmiss);

        btn_Ok  = findViewById(R.id.btn_Ok);
        list_miss = findViewById(R.id.list_miss);

        preferences = PreferenceManager.getDefaultSharedPreferences(this.getApplicationContext());
        editor = preferences.edit();





        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = dm.widthPixels;
        int height = dm.heightPixels;

        getWindow().setLayout((int)(width*.8),(int)(height*.3));




        GetMiss();


        list_miss.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(i!=0)
                {
                    //kayip_id = list_miss.getItemAtPosition(i).toString();
                    kayip_id = ids[i-1].toString();

                    editor.putString("kayip_id", kayip_id);
                    editor.commit();

                    btn_Ok.setEnabled(true);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        btn_Ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PostMiss();
                finish();
            }
        });

    }
    public void GetMiss() {
        controlSwitcheritem=2;
        HttpAsyncTask miss=new HttpAsyncTask();
//        miss.execute("http://192.168.43.127:8088/api/values/GetMiss");
        //miss.execute("http://192.168.10.73:8088/api/values/GetMiss");
        miss.execute("http://10.10.1.46:8088/api/values/GetMiss");

    }
    public void PostMiss() {
        controlSwitcheritem=1;

        HttpAsyncTask miss=new HttpAsyncTask();
        //miss.execute("http://192.168.10.73:8088/api/values/PostMissPause");
        miss.execute("http://10.10.1.46:8088/api/values/PostMissPause");
//        miss.execute("http://192.168.43.127:8088/api/values/PostMissPause");
    }




    public static String POST(String url, JSONObject jsonObject) {
        InputStream inputStream = null;
        String result = "";
        try {

            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(url);
            String json = "";
            json = jsonObject.toString();
            StringEntity se = new StringEntity(json,"UTF-8");
            httpPost.setEntity(se);
            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("Content-type", "application/json");
            HttpResponse httpResponse = httpclient.execute(httpPost);
            inputStream = httpResponse.getEntity().getContent();
            if (inputStream != null)
                result = convertInputStreamToString(inputStream);
            else
                result = "Did not work!";

        } catch (Exception e) {
            Log.d("InputStream", e.getLocalizedMessage());
        }
        return result;
    }

    private class HttpAsyncTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {

            JSONObject jsonObject = new JSONObject();

            if (controlSwitcheritem == 1)
            {
                try {

                    jsonObject.put("token", "a153dd6s33xv6uy9hgf23b16gh")
                            .put("id",id );

                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(popupMiss.this, "Connection Error", Toast.LENGTH_SHORT).show();
                }

                Log.d("JsonİnfoPost", jsonObject.toString());

            }
            else if (controlSwitcheritem == 2){
                try {
                    jsonObject.put("token", "a153dd6s33xv6uy9hgf23b16gh");
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(popupMiss.this, "Kayıp Zaman Hatası", Toast.LENGTH_SHORT).show();
                }

                Log.d("JsonİsemriPost", jsonObject.toString());

            }
            else {

                Toast.makeText(popupMiss.this, "Post Error", Toast.LENGTH_SHORT).show();

            }



            return POST(urls[0], jsonObject);
        }

        @Override
        protected void onPostExecute(String result) {

            try {

                if(controlSwitcheritem == 1) {
                    try {

                        Log.d("JsonMiss", result.toString());


                        if (Boolean.valueOf(result) == true) {
                            Toast.makeText(getApplicationContext(), "Bu Siparis Durakladı.", Toast.LENGTH_SHORT).show();

                            finish();

                        } else {
                            Toast.makeText(getApplicationContext(), "Duraklatılma Hatası", Toast.LENGTH_SHORT).show();
                        }
                    }
                    catch (Exception ex)
                    {
                        Toast.makeText(popupMiss.this, "Siparis Duarklatma Hatası", Toast.LENGTH_SHORT).show();
                    }
                }
                else {

                    Log.d("JsonİnfoMiss", result);

                    JSONObject jsonObject = new JSONObject(new String(result.getBytes("ISO-8859-1"), "UTF-8"));

                    JSONArray jsonArray = new JSONArray(jsonObject.getString("results"));

                    String Tanim_id[] = new String[jsonArray.length()];
                    ArrayList miss = new ArrayList<>();
                    miss.add("");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                        Tanim_id[i] = jsonObject1.get("id").toString();
                        miss.add(jsonObject1.get("name").toString());
                    }
                    ids = Tanim_id;
                    ArrayAdapter<String> adMiss = new ArrayAdapter<String>(popupMiss.this, android.R.layout.simple_spinner_dropdown_item, miss);
                    list_miss.setAdapter(adMiss);


                }

            } catch (JSONException e) {
                e.printStackTrace();
                Toast.makeText(popupMiss.this, "Bir Hata Oluştu Lütfen tekrar deneyin.", Toast.LENGTH_SHORT).show();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
    }


    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while ((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;
    }


    @Override
    public void onBackPressed() {

    }
}
