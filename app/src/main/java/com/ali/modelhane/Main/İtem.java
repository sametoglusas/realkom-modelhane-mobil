package com.ali.modelhane.Main;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.ali.modelhane.R;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class İtem extends AppCompatActivity {

//    TextView txt_order_info_type, txt_order_info_owner, txt_order_info_number;
    public static String txt_number, txt_owner, txt_type;/*sıparıs bılgıleri şimdi kullanılmıyor ama*/

    public String []cins;
    public String []urunler;

    Spinner list_cins, list_tur,list_once;


    CalendarView cal_date;
    public static String date;
    RadioButton rb_kolay, rb_orta, rb_zor;
    Button btn_designer_choice;
    OrdersFragment of = new OrdersFragment();

    public static int controlSwitcheritem;

    public static int workterm = 0 ;
    public static int Oncelik=1;
    public static String isemri ="";
    public static String urun ="";

    public static String zorluk = "Kolay";

    DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item);

        btn_designer_choice = findViewById(R.id.btn_designer_choice);

        list_cins = findViewById(R.id.list_cins);
        list_tur = findViewById(R.id.list_tur);
        list_once = findViewById(R.id.list_oncelik);

        cal_date = findViewById(R.id.cal_date);

        rb_kolay = findViewById(R.id.rb_kolay);
        rb_orta = findViewById(R.id.rb_orta);
        rb_zor = findViewById(R.id.rb_zor);

        /*Burası ilerde tablo istegi olup sonrada spinnere basılabılır.*/

        cinsAl();

        list_cins.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(i!=0)
                {
                    isemri = list_cins.getItemAtPosition(i).toString();

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });



        /**/

        list_tur.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int i, long id) {
                if(i!=0)
                {
                    urun = list_tur.getItemAtPosition(i).toString();

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });



        list_once.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Oncelik = Integer.valueOf(list_once.getItemAtPosition(position).toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        rb_kolay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rb_kolay.setChecked(true);
                rb_orta.setChecked(false);
                rb_zor.setChecked(false);
                zorluk = "Kolay";
            }
        });

        rb_orta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rb_kolay.setChecked(false);
                rb_orta.setChecked(true);
                rb_zor.setChecked(false);
                zorluk = "Orta";
            }
        });

        rb_zor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rb_kolay.setChecked(false);
                rb_orta.setChecked(false);
                rb_zor.setChecked(true);
                zorluk = "Zor";
            }
        });


        rb_kolay.setChecked(true);
        zorluk = "Kolay";


        long cur = cal_date.getDate();
        cal_date.setMinDate(cur);


        cal_date.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(@NonNull CalendarView view, int year, int month, int dayOfMonth) {

                date = (dayOfMonth+ "." + String.valueOf(Integer.valueOf( month)+1) + "." +year);

                Toast.makeText(İtem.this, date, Toast.LENGTH_LONG).show();

            }
        });

        btn_designer_choice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                controlSwitcheritem = 1;
                HttpAsyncTask İtem=new HttpAsyncTask();
                //İtem.execute("http://192.168.10.73:8088/api/uretimsure/GetTerm");
                İtem.execute("http://10.10.1.46:8088/api/uretimsure/GetTerm");
                //İtem.execute("http://192.168.43.127:8088/api/uretimsure/GetTerm");

//                Intent i = new Intent(İtem.this, ListDesigner.class);
//                startActivity(i);

            }
        });

    }

    public void cinsAl() {
        controlSwitcheritem=2;
        HttpAsyncTask CinsAl=new HttpAsyncTask();
        //CinsAl.execute("http://192.168.43.127:8088/api/values/GetWork");
        CinsAl.execute("http://10.10.1.46:8088/api/values/GetWork");
        //CinsAl.execute("http://192.168.10.73:8088/api/values/GetWork");
    }
    public void urunAl() {
        controlSwitcheritem=3;
        HttpAsyncTask UrunAl=new HttpAsyncTask();
        //CinsAl.execute("http://192.168.43.127:8088/api/values/GetWork");
        UrunAl.execute("http://10.10.1.46:8088/api/values/GetProduct");
        //CinsAl.execute("http://192.168.10.73:8088/api/values/GetWork");
    }



    public static String POST(String url, JSONObject jsonObject) {
        InputStream inputStream = null;
        String result = "";
        try {

            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(url);
            String json = "";
            json = jsonObject.toString();
            StringEntity se = new StringEntity(json,"UTF-8");
            httpPost.setEntity(se);
            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("Content-type", "application/json");
            HttpResponse httpResponse = httpclient.execute(httpPost);
            inputStream = httpResponse.getEntity().getContent();
            if (inputStream != null)
                result = convertInputStreamToString(inputStream);
            else
                result = "Did not work!";

        } catch (Exception e) {
            Log.d("InputStream", e.getLocalizedMessage());
        }
        return result;
    }

    private class HttpAsyncTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {

            JSONObject jsonObject = new JSONObject();

            if (controlSwitcheritem == 1)
            {
            try {
                jsonObject.put("token", "a153dd6s33xv6uy9hgf23b16gh")
                        .put("isemri", list_cins.getSelectedItem().toString())
                        .put("urun", list_tur.getSelectedItem().toString())
                        .put("zorluk", zorluk);



            } catch (JSONException e) {
                e.printStackTrace();
                Toast.makeText(İtem.this, "Aradıgınız İş Emri bulunamadı Lütfen İş Emrini Değiştirin", Toast.LENGTH_SHORT).show();
            }

            Log.d("JsonİnfoPost", jsonObject.toString());

            }
            else if (controlSwitcheritem == 2){
                try {
                    jsonObject.put("token", "a153dd6s33xv6uy9hgf23b16gh");
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(İtem.this, "İsemirleri Alınamadı", Toast.LENGTH_SHORT).show();
                }

                Log.d("JsonİsemriPost", jsonObject.toString());

            }

            else if (controlSwitcheritem == 3){
                try {
                    jsonObject.put("token", "a153dd6s33xv6uy9hgf23b16gh");
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(İtem.this, "Ürünler Alınamadı", Toast.LENGTH_SHORT).show();
                }

                Log.d("JsonUrunPost", jsonObject.toString());

            }
            else {

                Toast.makeText(İtem.this, "Post Error", Toast.LENGTH_SHORT).show();

            }



            return POST(urls[0], jsonObject);
        }

        @Override
        protected void onPostExecute(String result) {

            if(controlSwitcheritem == 1) {
                try{

                    workterm = Integer.valueOf(result);

                    Log.d("JsonİnfoPostResult", result);


                    Intent i = new Intent(getApplicationContext(), ListDesigner.class);
                    startActivity(i);

                }
                catch(Exception ex){

                    Toast.makeText(İtem.this, "Tanımladıgınız İs emri Bulunamadı", Toast.LENGTH_SHORT).show();

                }
                
            }
            else if(controlSwitcheritem == 2) {
                try {

                    Log.d("Jsonİnfoİsemri", result);

                    JSONObject jsonObject = new JSONObject(new String(result.getBytes("ISO-8859-1"), "UTF-8"));

                    JSONArray jsonArray = new JSONArray(jsonObject.getString("results"));

                    String İsemri_ıd[]=new String[jsonArray.length()];
                    ArrayList İsemri=new ArrayList<>();
                    İsemri.add("");
                    for (int i=0;i<jsonArray.length();i++){
                        JSONObject jsonObject1=jsonArray.getJSONObject(i);
                        İsemri_ıd[i]=jsonObject1.get("id").toString();
                        İsemri.add(jsonObject1.get("name").toString());
                    }
                    cins=İsemri_ıd;
                    ArrayAdapter <String> adCinsler= new ArrayAdapter<String>(İtem.this,android.R.layout.simple_spinner_dropdown_item,İsemri);
                    list_cins.setAdapter(adCinsler);


                    urunAl();

                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(İtem.this, "İsemrinde Bir Hata Oluştu", Toast.LENGTH_SHORT).show();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }

            else if (controlSwitcheritem == 3) {
                try {

                    Log.d("JsonİnfoUrun", result);

                    JSONObject jsonObject = new JSONObject(new String(result.getBytes("ISO-8859-1"), "UTF-8"));

                    JSONArray jsonArray = new JSONArray(jsonObject.getString("results"));

                    String urun_id[]=new String[jsonArray.length()];
                    ArrayList Urun=new ArrayList<>();
                    Urun.add("");
                    for (int i=0;i<jsonArray.length();i++){
                        JSONObject jsonObject1=jsonArray.getJSONObject(i);
                        urun_id[i]=jsonObject1.get("id").toString();
                        Urun.add(jsonObject1.get("name").toString());
                    }
                    urunler=urun_id;
                    ArrayAdapter <String> adUrunler= new ArrayAdapter<String>(İtem.this,android.R.layout.simple_spinner_dropdown_item,Urun);
                    list_tur.setAdapter(adUrunler);


                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(İtem.this, "Üründe Bir Hata Oluştu", Toast.LENGTH_SHORT).show();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }

            else
                Toast.makeText(İtem.this, "Baglantınızı Kontrol Edin", Toast.LENGTH_SHORT).show();
        }
    }

    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while ((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;
    }

//    @Override
//    public void onBackPressed() {
//
//    }



}
