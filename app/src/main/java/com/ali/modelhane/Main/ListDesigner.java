package com.ali.modelhane.Main;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.ali.modelhane.Helper.ListAdapter;
import com.ali.modelhane.Helper.ListDesignersAdapter;
import com.ali.modelhane.Helper.ListPopupAdapter;
import com.ali.modelhane.Helper.RecyclerItemClickListener;
import com.ali.modelhane.Helper.pGS;
import com.ali.modelhane.R;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;

public class ListDesigner extends AppCompatActivity {

    RecyclerView listview;
    ListDesignersAdapter adapter;
    public static int controlSwitcher = 0;
    public static int change = 0;

    public static int list_designer_id = 0;
    public static int Percent_of_fullness = 0;
    public static int degree = 0;



    String date, time;

    OrdersFragment of = new OrdersFragment();
    ContinuesFragment cf = new ContinuesFragment();

    İtem item = new İtem();

    public static String[] ids;

    public static String[] dd_3;
    public static String[] dd_2;
    public static String[] dd_1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_designer);

        listview = findViewById(R.id.list_designers);

        listdesigners();
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy hh:mm:ss");


        Calendar calendar = Calendar.getInstance();
        String currentdate = String.valueOf(sdf.format(calendar.getTime()));

        //date = currentdate.substring(0, 10).trim();
        time = currentdate.substring(10, 19).trim();

        listview.addOnItemTouchListener(
                new RecyclerItemClickListener(this, listview, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        list_designer_id = Integer.valueOf((ids[position]));
                        listworkfull();

                        Toast.makeText(ListDesigner.this, "Modelistin Görevleri listeleniyor...", Toast.LENGTH_SHORT).show();

                    }

                    @Override
                    public void onLongItemClick(View view, final int position) {

                        AlertDialog.Builder builder = new AlertDialog.Builder(ListDesigner.this);
                        builder.setTitle("Modelist Seçtiniz");
                        builder.setMessage("Görev Verilsin mi?");
                        builder.setNegativeButton("HAYIR", new DialogInterface.OnClickListener(){
                            public void onClick(DialogInterface dialog, int id) {

                                Toast.makeText(ListDesigner.this, "İşlem İptal Edildi.", Toast.LENGTH_SHORT).show();

                            }
                        });


                        builder.setPositiveButton("EVET", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                Toast.makeText(ListDesigner.this, "Modeliste Görevi Gönderildi", Toast.LENGTH_SHORT).show();

                                list_designer_id = Integer.valueOf((ids[position]));

                                choice_designer(change);


                            }
                        });


                        builder.show();
                    }
                })
        );


    }



    public void listdesigners() {

        controlSwitcher = 0;
        HttpAsyncTask listalldesigners = new HttpAsyncTask();
        //listalldesigners.execute("http://192.168.10.73:8088/api/values/Alldesigner");
        listalldesigners.execute("http://10.10.1.46:8088/api/values/Alldesigner");
        //listalldesigners.execute("http://192.168.43.127:8088/api/values/WorkfullReturn");

//        ilanlistele();




    }

    public void listworkfull() {

        controlSwitcher = 3;
        HttpAsyncTask listworkfull = new HttpAsyncTask();
        //listworkfull.execute("192.168.10.73:8088/api/values/WorkfullReturn");
        listworkfull.execute("http://10.10.1.46:8088/api/values/WorkfullReturn");
        //listalldesigners.execute("http://192.168.43.127:8088/api/values/WorkfullReturn");

//        ilanlistele();

    }

    public void choice_designer(int change) {

        this.change = change;
        if (change == 0)
        {
            controlSwitcher = 1;
            HttpAsyncTask ListDesigner = new HttpAsyncTask();
            //ListDesigner.execute("http://192.168.10.73:8088/api/values/DesignerGetOrderByAdmin");
            ListDesigner.execute("http://10.10.1.46:8088/api/values/DesignerGetOrderByAdmin");
            //ListDesigner.execute("http://192.168.43.127:8088/api/values/DesignerGetOrderByAdmin");
//        Intent i = new Intent(ListDesigner.this, ListActivity.class);
//        startActivity(i);

        }
        else
        {
            controlSwitcher = 2;
            HttpAsyncTask ListDesigner = new HttpAsyncTask();
            //ListDesigner.execute("http://192.168.10.73:8088/api/values/GetOrderByAdminChangeDesigner");
            ListDesigner.execute("http://10.10.1.46:8088/api/siparisler/GetOrderByAdminChangeDesigner");
            //ListDesigner.execute("http://192.168.43.127:8088/api/values/DesignerGetOrderByAdmin");
//        Intent i = new Intent(ListDesigner.this, ListActivity.class);
//        startActivity(i);
        }

    }




    public static String POST(String url, JSONObject jsonObject) {
        InputStream inputStream = null;
        String result = "";
        try {

            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(url);
            String json = "";
            json = jsonObject.toString();
            StringEntity se = new StringEntity(json, "UTF-8");
            httpPost.setEntity(se);
            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("Content-type", "application/json");
            HttpResponse httpResponse = httpclient.execute(httpPost);
            inputStream = httpResponse.getEntity().getContent();
            if (inputStream != null)
                result = convertInputStreamToString(inputStream);
            else
                result = "Did not work!";

        } catch (Exception e) {
            Log.d("InputStream", e.getLocalizedMessage());
        }
        return result;
    }

    private class HttpAsyncTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {

            JSONObject jsonObject = new JSONObject();

            if (controlSwitcher == 0) {

                try {
                    jsonObject.put("token", "a153dd6s33xv6uy9hgf23b16gh")
                            .put("tarih", item.date);

                    /*simdilik tum designers cekildi*/

                    Log.d("JsonAlldesigner", jsonObject.toString());


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else if (controlSwitcher == 1) {
                try {
                    jsonObject.put("token", "a153dd6s33xv6uy9hgf23b16gh")
                            .put("designer_id", list_designer_id)
                            .put("siparis_id", of.list_item_id)
                            .put("isemri", item.isemri)
                            .put("urun", item.urun)
                            .put("zorluk", item.zorluk)
                            .put("oncelik", item.Oncelik)
                            .put("date", item.date)
                            .put("time", time);
                    /**/

                    Log.d("JsonDesignerWork", jsonObject.toString());


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            else if (controlSwitcher == 2) {
                try {
                    jsonObject.put("token", "a153dd6s33xv6uy9hgf23b16gh")
                            .put("designer_id", list_designer_id)
                            .put("isemri_id", cf.list_item_id);

                    Log.d("JsonDesignerChangeWork", jsonObject.toString());


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            else if (controlSwitcher == 3) {

                try {
                    jsonObject.put("token", "a153dd6s33xv6uy9hgf23b16gh")
                            .put("tarih", item.date)
                            .put("id",list_designer_id);
                    /*simdilik tum designers cekildi*/

                    Log.d("JsonAllworkfull", jsonObject.toString());


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            else {
                Toast.makeText(ListDesigner.this, "HttpAsyncTask Error", Toast.LENGTH_SHORT).show();
            }

            return POST(urls[0], jsonObject);
        }

        @Override
        protected void onPostExecute(String result) {

            if (controlSwitcher == 0)

            {
                Log.d("Jsonalldesigner", result);


                try {


                    JSONObject jsonObject = new JSONObject(new String(result.getBytes("ISO-8859-1"), "UTF-8"));


                    JSONArray jsonArray = new JSONArray(jsonObject.getString("results"));

                    Log.d("Jsondesigner", jsonArray.toString());


                    try {

//                        String[] dd2 = new String[jsonArray.length()];
                        String[] dd1 = new String[jsonArray.length()];

                        if (jsonArray.length() > 0) {
                            String[] idler = new String[jsonArray.length()];

                            Log.d("Json", "deneme");


                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                                Log.d("Json", "deneme");


                                dd1[i] = jsonObject1.getString("adiSoyadi");
//                                dd2[i] = jsonObject1.getString("workfull");


                                idler[i] = jsonObject1.getString("personelID");

                            }
//                            dd_2 = dd2;
                            dd_1 = dd1;
                            ids = idler;

                            //listworkfull();
                            ilanlistele();

                        } else {
                            Toast.makeText(getApplicationContext(), "Sectiginiz Tarihte Modelist yoktur.", Toast.LENGTH_SHORT).show();
                        }

                    } catch (Exception e) {
                        Toast.makeText(getApplicationContext(), "Modelist Bilgilerini Kontrol Edin", Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Baglantınızı Kontrol Edin", Toast.LENGTH_SHORT).show();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }


            }


            else if (controlSwitcher == 1)
            {
                Log.d("JsonAtama", result.toString());


                if (result.equals("true"))
                {
                    Toast.makeText(getApplicationContext(), "Modeliste Görevi Teslim Edildi.", Toast.LENGTH_SHORT).show();

                    /*buraya hem designerın hem işin id si gonderilsin sonra da result donsun  belki iş idsine gerek kalmaz*/

                    Intent i = new Intent(ListDesigner.this, ListActivity.class);
                    startActivity(i);

                } else {
                    Toast.makeText(getApplicationContext(), "Lütfen Tekrar Deneyin.", Toast.LENGTH_SHORT).show();
                }


            }

            else if (controlSwitcher == 2)
            {
                Log.d("JsonAtamaDegistirme", result.toString());


                if (result.equals("true"))
                {
                    Toast.makeText(getApplicationContext(), "Modeliste Görevi Teslim Edildi.", Toast.LENGTH_SHORT).show();

                    /*buraya hem designerın hem işin id si gonderilsin sonra da result donsun  belki iş idsine gerek kalmaz*/

                    Intent i = new Intent(ListDesigner.this, ListActivity.class);
                    startActivity(i);

                } else {
                    Toast.makeText(getApplicationContext(), "Lütfen Tekrar Deneyin.", Toast.LENGTH_SHORT).show();
                }


            }

            else if (controlSwitcher == 3) {
                Log.d("Jsonalldesignerworkfull", result);

                try{

                    Percent_of_fullness = Integer.valueOf(result);

                    Log.d("JsonİnfoPostResult", result);

                        Percent_of_fullness = (int) Math.round(((Percent_of_fullness)/540.0)*100.0);
                        degree = (int) Math.round(((Integer.valueOf(result) + item.workterm)/540.0)*100.0);

                    startActivity(new Intent(ListDesigner.this, popup.class));


//                Toast.makeText(ListDesigner.this, result, Toast.LENGTH_SHORT).show();
//                Toast.makeText(ListDesigner.this,   Percent_of_fullness+" yok "+degree, Toast.LENGTH_SHORT).show();

                }
                catch(Exception ex){

                    Toast.makeText(ListDesigner.this, "Sectiginiz Modelistin Doluluk oranı Bulunamadı", Toast.LENGTH_SHORT).show();
                }

            }

            else  {
                Toast.makeText(ListDesigner.this, "onPostExecute Error", Toast.LENGTH_SHORT).show();
            }

        }
    }

    public void ilanlistele() {

        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT);
        LinearLayoutManager llm = new LinearLayoutManager(this.getApplicationContext());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        listview.setLayoutManager(llm);
        listview.setLayoutParams(params);
        listview.setHasFixedSize(true);

        adapter = new ListDesignersAdapter(dd_1);
        //adapter = new ListDesignersAdapter(dd_1,dd_2);
        listview.setAdapter(adapter);

        listview.addItemDecoration(new DividerItemDecoration(this.getApplicationContext(), LinearLayoutManager.VERTICAL));


    }


    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while ((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;
    }




}
