package com.ali.modelhane.Main;

import android.content.Intent;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.ali.modelhane.Helper.ViewPagerAdapter;
import com.ali.modelhane.R;

import java.util.Random;


public class DesignerActivity extends AppCompatActivity {

    TabLayout tabLayout;
    ViewPager viewPager;
    ViewPagerAdapter viewPagerAdapter;
    int IDviewPager=0;
    SwipeRefreshLayout swipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_designer);
        tabLayout =(TabLayout)findViewById(R.id.tabLayout);
        viewPager =(ViewPager)findViewById(R.id.viewPager);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe);


        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPagerAdapter.addFragments(new WorksFragment(),"İşler");
//        viewPagerAdapter.addFragments(new Work_contFragment(),"Devam");

        viewPager.setAdapter(viewPagerAdapter);
        tabLayout.setupWithViewPager(viewPager);





        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {
                IDviewPager=viewPager.getCurrentItem();

                if(IDviewPager==0)
                {
                    WorksFragment.type_id="Yeni";
                }
//                if(IDviewPager==1)
//                {
//                    Work_contFragment.type_id="Duraklatıldı";
//                }
//                if(IDviewPager==2)
//                {
//                    List_vet.type_id=3;
//                }
//                if(IDviewPager==3)
//                {
//                    List_work.type_id=4;
//                }
            }
        });



        swipeRefreshLayout.setColorSchemeResources(R.color.orderBack,R.color.ordercell,R.color.orderBorder);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(true);

                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        swipeRefreshLayout.setRefreshing(false);

                        int min = 50;
                        int max = 100;

                        Random random = new Random();
                        int i = random.nextInt(max - min + 1)+min;


                        Intent in = new Intent(getApplicationContext(), DesignerActivity.class);
                        startActivity(in);

                        //finish();
                        //onStart();
                    }
                },3000);
            }
        });

    }

    @Override
    public void onBackPressed() {

    }
}