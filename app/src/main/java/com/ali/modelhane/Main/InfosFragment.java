package com.ali.modelhane.Main;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.ali.modelhane.Helper.ListAdapter;
import com.ali.modelhane.Helper.RecyclerItemClickListener;
import com.ali.modelhane.R;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;


public class InfosFragment extends Fragment {

    RecyclerView listView;
    ListAdapter adapter;
    public static String type_id="Devam";
    public static String [] ids;
    public static int controlSwitcher = 0;

    /**/
    String ara;
    /**/
    public static int list_item_id=0;


    public static String [] od_2;
    public static String [] od_3;
    public static String [] od_4;

    public InfosFragment() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_infos, container, false);

        listView = (RecyclerView)view.findViewById(R.id.list_orders);

        ListOrders();

        listView.addOnItemTouchListener(
                new RecyclerItemClickListener(this.getContext(), listView ,new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        try {

                            list_item_id = Integer.valueOf(ids[position]);
                            TakeAnno();
                        }
                        catch (Exception e){

                        }
                        Log.d("Jsonitemid", String.valueOf(list_item_id));

                    }
                    @Override
                    public void onLongItemClick(View view, int position) {
                    }
                })
        );

        return view;
    }




    public void ilanlistele() {

        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT);
        LinearLayoutManager llm = new LinearLayoutManager(this.getContext());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        listView.setLayoutManager(llm);
        listView.setLayoutParams(params);
        listView.setHasFixedSize(true);

        adapter = new ListAdapter(od_2,od_3,od_4,"Askida");
        listView.setAdapter(adapter);

        listView.addItemDecoration(new DividerItemDecoration(this.getContext(), LinearLayoutManager.VERTICAL));


    }

    public void ListOrders() {
        controlSwitcher=1;
        HttpAsyncTask listAllAnno = new HttpAsyncTask();
        //listAllAnno.execute("http://192.168.10.73:8088/api/siparisler/GetOrdersByAdmin");
        listAllAnno.execute("http://10.10.1.46:8088/api/siparisler/GetOrdersByAdmin");
        //listAllAnno.execute("http://192.168.43.127:8088/api/siparisler/GetOrdersByAdmin");
    }

    public void TakeAnno() {
        controlSwitcher=2;

        AlertDialog.Builder builder = new AlertDialog.Builder(InfosFragment.this.getContext());
        builder.setTitle("Bilgi Eksikliği Giderme");
        builder.setMessage("Bu siparişin Eksiğini Gidermek istediğinize emin misiniz?!!!");
        builder.setNegativeButton("HAYIR", new DialogInterface.OnClickListener(){
            public void onClick(DialogInterface dialog, int id) {

                Toast.makeText(InfosFragment.this.getContext(), "İşlem İptal Edildi.", Toast.LENGTH_SHORT).show();

            }
        });


        builder.setPositiveButton("EVET", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                Toast.makeText(InfosFragment.this.getContext(), "Siparis Bilgileri listeleniyor.", Toast.LENGTH_SHORT).show();

                HttpAsyncTask listAllAnno = new HttpAsyncTask();
                //listAllAnno.execute("http://192.168.10.73:8088/api/values/Getinfoorder");
                listAllAnno.execute("http://10.10.1.46:8088/api/values/Getinfoorder");
                //listAllAnno.execute("http://192.168.43.127:8088/api/values/Getinfoorder");

                // aynı modelist sayfası gibi sadece listele ve ata oradan isemri ve modelist idlerini assagıdaki fonksiyona at
                //listAllAnno.execute("http://10.10.1.41:8088/api/siparisler/GetOrderByAdminChangeDesigner");
                //listAllAnno.execute("http://192.168.43.127:8088/api/siparisler/GetOrderByAdminChangeDesigner");

            }
        });


        builder.show();


    }


    public static String POST(String url, JSONObject jsonObject) {
        InputStream inputStream = null;
        String result = "";
        try {

            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(url);
            String json = "";
            json = jsonObject.toString();
            StringEntity se = new StringEntity(json,"UTF-8");
            httpPost.setEntity(se);
            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("Content-type", "application/json");
            HttpResponse httpResponse = httpclient.execute(httpPost);
            inputStream = httpResponse.getEntity().getContent();
            if (inputStream != null)
                result = convertInputStreamToString(inputStream);
            else
                result = "Did not work!";

        } catch (Exception e) {
            Log.d("InputStream", e.getLocalizedMessage());
        }
        return result;
    }

    private class HttpAsyncTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {

            JSONObject jsonObject=new JSONObject();

            if(controlSwitcher==1){
                try {

                    jsonObject.put("token","a153dd6s33xv6uy9hgf23b16gh")
                            .put("siparisdurum",type_id)
                            .put("isemridurum","Askida");


                    Log.d("JsonAllOrdersEksik",jsonObject.toString());


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            else
            {
                try {

                    jsonObject.put("token","a153dd6s33xv6uy9hgf23b16gh")
                            .put("id",list_item_id);


                    Log.d("JsonAllOrdersEksikid",jsonObject.toString());


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            return POST(urls[0], jsonObject);
        }
        @Override
        protected void onPostExecute(String result) {



            if (controlSwitcher == 1) {

//                    Toast.makeText(getContext(), result, Toast.LENGTH_SHORT).show();
                Log.d("JsonResultAllOrder",result);


                try {


                    JSONObject jsonObject = new JSONObject(new String(result.getBytes("ISO-8859-1"), "UTF-8"));


                    JSONArray jsonArray = new JSONArray(jsonObject.getString("results"));

                    try {
                        String[] od2 = new String[jsonArray.length()];
                        String[] od3 = new String[jsonArray.length()];
                        String[] od4 = new String[jsonArray.length()];


                        if (jsonArray.length() > 0) {
                            String[] idler = new String[jsonArray.length()];


                            for (int i = 0; i < jsonArray.length(); i++) {

                                JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                                Log.d("JsonBurada",jsonObject1.toString());

                                od2[i] = jsonObject1.getString("musteri").toString();
                                od3[i] = jsonObject1.getString("model_no").toString();
                                od4[i] = jsonObject1.getString("model_adi").toString();


                                idler[i] = jsonObject1.getString("calisma_id");

                            }
                            od_2 = od2;
                            od_3 = od3;
                            od_4 = od4;

                            ilanlistele();
                            ids = idler;
                        } else {
                            Toast.makeText(InfosFragment.this.getContext(), "Bilgileri Eksik olan Sipariş Yok", Toast.LENGTH_SHORT).show();
                        }
                    }
                    catch (Exception ex)
                    {
                        Toast.makeText(InfosFragment.this.getContext(), "Siparis Bilgilerinizi Kontrol Edin", Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(InfosFragment.this.getContext(), "Baglantınızı Kontrol Edin", Toast.LENGTH_SHORT).show();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
            else if (controlSwitcher == 2) {

                Log.d("JsonResultOneOrder",result);


                try {


                    JSONObject jsonObject = new JSONObject(new String(result.getBytes("ISO-8859-1"), "UTF-8"));


                    JSONArray jsonArray = new JSONArray(jsonObject.getString("results"));
                    Info info = new Info();

                    try {


                        if (jsonArray.length() > 0) {
                            String[] idler = new String[jsonArray.length()];




                                JSONObject jsonObject1 = jsonArray.getJSONObject(0);


                                info.model_no = (jsonObject1.getString("model_no").toString());
                                info.order_no = (jsonObject1.getString("order_no").toString());
                                info.model_name = (jsonObject1.getString("model_name").toString());
                                info.aciklama = (jsonObject1.getString("aciklama").toString());

                                Log.d("JsonBurada",jsonObject1.toString());


                                idler[0] = jsonObject1.getString("calisma_id");
                                ids = idler;

                            Intent i = new Intent(InfosFragment.this.getContext(), Info.class);
                            startActivity(i);

                        } else {
                            Toast.makeText(InfosFragment.this.getContext(), "Bilgileri Eksik Sipariş Yok", Toast.LENGTH_SHORT).show();
                        }
                    }
                    catch (Exception ex)
                    {
                        Toast.makeText(InfosFragment.this.getContext(), "Siparis Bilgilerini Kontrol Edin", Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(InfosFragment.this.getContext(), "Baglantınızı Kontrol Edin", Toast.LENGTH_SHORT).show();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }

            else {
                Toast.makeText(InfosFragment.this.getContext(), "Connection Error 404", Toast.LENGTH_SHORT).show();
            }

        }
    }

    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while ((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;
    }
}
